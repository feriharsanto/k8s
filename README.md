# Step by Step Deployment Tyk Self Manage to k8s

## Issue in redis
```Error: failed to download "bitnami/redis" (hint: running `helm repo update` may help)```    
Tambahkan repository bitnami dan tyk agar helm bisa mengakses  
`helm repo add bitnami https://charts.bitnami.com/bitnami`    
`helm repo add tyk-helm https://helm.tyk.io/public/helm/charts`   
`helm repo update`  

## Tambahkan namespace baru
`kubectl create namespace tyk-dev`

## Export configurasi helm tyk
`helm show values tyk-helm/tyk-pro > values.yaml`  

## Install Redis
`helm install tyk-redis bitnami/redis -n tyk-dev`   
`export REDIS_PASSWORD=$(kubectl get secret --namespace tyk tyk-redis -o jsonpath="{.data.redis-password}" | base64 --decode)`  

## Install Redis standalone
`helm install tyk-redis bitnami/redis --set architecture=standalone -n tyk-dev --kubeconfig ~/.kube/tyk-dev-alicloud-controlplane`

## Install Tyk Control Plane
Ubah `values.yml`, check:   
- `secrets.APISecret`   
- `secrets.AdminSecret`   
- `redis.addrs` sesuaikan dengan nilai `REDIS_PASSWORD`   
- `redis.enableCluster` sesuaikan dengan redis instalasi, bila menggunakan cluster set dengan nilai true, bila master-slave masukan false   
- `mongo.mongoURL` masukan dsn mongodb (hanya untuk control plane)   
- `mdcb.enabled` true (hanya untuk control plane)   
- `tib.enabled` true (hanya untuk control plane)   
- `dash.enabled` true (hanya untuk control plane)   
- `dash.license` nilai lisensi (hanya untuk control plane)   
- `portal.bootstrap` set true, jika Master and Dashboard bootstrap adalah true, set false bila tidak ada rencana menggunakan developer portal   
- `gateway.enabled` true     
- `pump.enabled` true (hanya untuk control plane)   

`helm install tyk-pro tyk-helm/tyk-pro -f ./values.yaml -n tyk-dev --kubeconfig ~/.kube/tyk-dev-alicloud-controlplane --wait`  

## Change value of licences
`helm upgrade -f base/licence.yaml tyk-pro tyk-helm/tyk-pro -n tyk-dev --kubeconfig ~/.kube/tyk-dev-alicloud-controlplane --wait`  

## Add MDCB installation
`helm upgrade -f base/tyk-values.yaml tyk-pro tyk-helm/tyk-pro -n tyk-dev --kubeconfig ~/.kube/tyk-dev-alicloud-controlplane`

## Slave gateway installation
`kubectl create namespace tyk-dev`  
folder `slave-tpl` adalah hasil generate-an dari helm template
```sh
helm template \
    --name-template tyk-pro \
    --output-dir slave-tpl \
    --namespace tyk-dev \
    --values base/tyk-slave.yaml \
    tyk-helm/tyk-pro
```
Ubah `slave-tpl/configmap-gateway.yaml` 
`"use_db_app_configs": false,`

Selanjutnya, dapat di pastikan policy load dan alaytics pump menggunakan RPC driver:

```sh
"policies": {
  "policy_source": "rpc",
  "policy_record_name": "tyk_policies"
},
"analytics_config": {
  "type": "rpc",
  ... // remains the same
},
```

Terakhir, fitur slave options harus di isi
```sh
"slave_options": {
  "use_rpc": true,
  "rpc_key": "{ORGID}",
  "api_key": "{APIKEY}",
  "connection_string": "{MDCB_HOSTNAME:9091}",
  "enable_rpc_cache": true,
  "bind_to_slugs": false,
  "group_id": "{ny}",
  "use_ssl": false,
  "ssl_insecure_skip_verify": true
},
"auth_override": {
  "force_auth_provider": true,
  "auth_provider": {
    "name": "",
    "storage_engine": "rpc",
    "meta": {}
  }
}
```  
`{ny}` pastikan konsisten utk gateway yang berasal dari cluster dan dc yang sama, contoh: `gcp-jkt` or `aliyun-jkt`

Apply configurasi ke slave cluster
`kubectl apply -f slave/tyk-pro/templates/ -n tyk-dev`